package ng.chat.tests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.wellmanage.ic.IncidentCentral;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = IncidentCentral.class)
@WebAppConfiguration
public class PrototypeApplicationTests {

	@Test
	public void contextLoads() {
	}

}