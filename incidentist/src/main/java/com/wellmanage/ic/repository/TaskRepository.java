package com.wellmanage.ic.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wellmanage.ic.domain.Task;

public interface TaskRepository extends JpaRepository<Task, Integer> {

}