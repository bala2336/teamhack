package com.wellmanage.ic.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wellmanage.ic.domain.Chat;
import com.wellmanage.ic.repository.ChatRepository;

@RestController
@RequestMapping("/api/chats")
public class ChatController {

	@Autowired
	private ChatRepository repo;

	@RequestMapping(method = RequestMethod.GET)
	public List<Chat> findItems() {
		return repo.findAll();
	}

	@RequestMapping(method = RequestMethod.POST)
	public Chat addItem(@RequestBody Chat item) {
		item.setId(null);
		return repo.saveAndFlush(item);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void deleteItem(@PathVariable Integer id) {
		repo.delete(id);
	}

	@RequestMapping(value = "/tagChat")
	public void tagChat(@RequestBody Chat chat) {
		Chat chatToTag = repo.findOne(chat.getId());
		if (chatToTag.getTaggedEvent() == null) {
			chatToTag.setTaggedEvent("tagged");
		} else {
			chatToTag.setTaggedEvent(null);
		}
		repo.save(chatToTag);
	}

	@RequestMapping(value = "/byIncident/{incidentId}")
	public List<Chat> getChatsByIncident(@PathVariable String incidentId) {
		return repo
				.findAll()
				.stream()
				.filter(chats -> chats.getIncidentId() != null
						&& chats.getIncidentId().equalsIgnoreCase(incidentId))
				.collect(Collectors.toList());

	}

}
