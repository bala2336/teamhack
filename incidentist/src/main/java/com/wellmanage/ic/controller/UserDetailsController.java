package com.wellmanage.ic.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.wellmanage.ic.security.UserSecurityUtils;
import com.wellmanage.ic.servicenow.client.api.ServiceNowDataAccessorService;
import com.wellmanage.ic.servicenow.client.exception.BadRequestException;
import com.wellmanage.ic.servicenow.client.exception.InvalidArgumentsException;
import com.wellmanage.weos.sso.hantweb.HantwebUser;

@RestController
public class UserDetailsController {

	/** The user security utils. */
	@Autowired
	private UserSecurityUtils userSecurityUtils;

	@Autowired
	private ServiceNowDataAccessorService snda;
	
	@RequestMapping(value = "api/loggedInUser")
	public HantwebUser getLoggedInUserDetails(@AuthenticationPrincipal HantwebUser user) {
		return user;
	}

	@RequestMapping(value = "api/snUser")
	@ResponseBody
	public String getServiceNowUser(@RequestParam String userId) throws BadRequestException, InvalidArgumentsException {
		return snda.getSysUser(userId).getBody();
		
	}
	
	
}