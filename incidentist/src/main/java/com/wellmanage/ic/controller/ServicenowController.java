package com.wellmanage.ic.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.wellmanage.ic.dto.SimpleNotifier;
import com.wellmanage.ic.servicenow.client.api.ServiceNowDataAccessorService;
import com.wellmanage.ic.servicenow.client.exception.BadRequestException;
import com.wellmanage.ic.servicenow.client.exception.InvalidArgumentsException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import org.apache.commons.codec.binary.Base64;
@RestController
public class ServicenowController {

	
	@Autowired
	private ServiceNowDataAccessorService sndas;
	@RequestMapping(value="/api/serviceNow/tasks",method = RequestMethod.GET)
	@ResponseBody
	public String findItems(@RequestParam String incidentId) throws Exception  {
		
		return sndas.getIncidentTasks(incidentId).getBody();
	
	}

	@RequestMapping(value="/api/serviceNow/findUser",method = RequestMethod.GET)
	@ResponseBody
	public String findUser(@RequestParam String userPattern) throws Exception  {
		
		return sndas.findUser(userPattern).getBody();
	
	}	

	@RequestMapping(value="/api/serviceNow/criticalIncidents",method = RequestMethod.GET)
	@ResponseBody
	public String criticalIncidents() throws Exception  {
		
		return sndas.getCriticalIncidents().getBody();
	
	}		
	
	
	@RequestMapping(value="/api/sendXmatters",method = RequestMethod.GET)
	@ResponseBody
	public String sendNotification(String messgage){
		RestTemplate restTemplate = new RestTemplate();
		String xmaterrsGateway="https://wellington-np.hosted.xmatters.com/reapi/2015-04-01/forms/922dbf03-358d-46d8-9ac4-e8d12798071b/triggers";
		String plainCreds = "codered:hackthisifyoucan";
		byte[] plainCredsBytes = plainCreds.getBytes();
		byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
		String base64Creds = new String(base64CredsBytes);
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Basic " + base64Creds);
		headers.add("Content-Type", "application/json");
		SimpleNotifier sn= new SimpleNotifier();
		Gson gson = new Gson();
		System.out.println(gson.toJson(sn));
		HttpEntity<String> request = new HttpEntity<String>(gson.toJson(sn),headers);
		restTemplate.postForObject(xmaterrsGateway,request,String.class);
		return "Noticiation has been send";	
	}
}
