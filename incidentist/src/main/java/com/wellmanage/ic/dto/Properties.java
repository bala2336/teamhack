package com.wellmanage.ic.dto;

import com.google.gson.annotations.SerializedName;

public class Properties {
	
	@SerializedName("Message Text")
	private String messageText;
	
	
	public String getMessageText() {
		return messageText;
	}
	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}
	
	
}
