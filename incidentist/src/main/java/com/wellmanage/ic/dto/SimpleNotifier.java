package com.wellmanage.ic.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SimpleNotifier {
	@JsonProperty("properties")
	Properties  properties = new Properties();
	List<Map<String,String>> recipients = new ArrayList<>();
	public SimpleNotifier(){
		HashMap<String,String> people= new HashMap<String,String>();
		people.put("targetName","venkak");
		HashMap<String,String> people1= new HashMap<String,String>();

		people1.put("targetName","naraya");
		recipients.add(people);
		recipients.add(people1);

		properties.setMessageText("Testing fron New Incident management System, there no issue so just chillout");
	}
	public Properties getMessageProperties() {
		return properties;
	}
	public void setMessageProperties(Properties messageProperties) {
		this.properties = messageProperties;
	}
	public List<Map<String, String>> getRecipients() {
		return recipients;
	}
	public void setRecipients(List<Map<String, String>> recipients) {
		this.recipients = recipients;
	}
}
