package com.wellmanage.ic.database;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;


public class IncidentAppMongoUtil {

	private static final String DB_NAME = "incidentist";

	private static Mongo mongo;
	private static DB incidentistDB; 
	
	static {
		try {
			mongo = new Mongo("localhost", 27017);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		incidentistDB = mongo.getDB(DB_NAME);
	}
	
	
	public static Collection<String> getDatabaseNames() {
		Collection<String> dbs = mongo.getDatabaseNames();
		
		return dbs;
	}

	public static DBCollection getCollection(String collectionName) {
		return incidentistDB.getCollection(collectionName);
	}
	
	public static String getIncident(String incidentNumber) {
		DBCollection incidentCollection = incidentistDB.getCollection("incidents");
		
		DBCursor cursor = incidentCollection.find(new BasicDBObject("incidentId",incidentNumber));
		
		
		IncidentBean incident = null;
		DBObject dbObject = null;
		while (cursor.hasNext()) {
			
			dbObject = cursor.next();
			
			
/*			System.out.println("Object from Mongo :::"+ dbObject);
			
			incident = new IncidentBean();
			String incidentId = (String)dbObject.get("incidentId");
			incident.setIncidentId(incidentId);

			if (null != dbObject.get("summaries")) {
				BasicDBList  summaryList = (BasicDBList)dbObject.get("summaries");
				
				Collection<SummaryBean> summaries = new ArrayList<SummaryBean>();
				BasicDBObject summaryObj = null;
				SummaryBean summaryBean = null;
				for (int i=0;i<summaryList.size();i++) {
					
					summaryObj = (BasicDBObject)summaryList.get(i);
					if (null != summaryObj) {
						summaryBean = new SummaryBean();
						summaryBean.setSummaryText(summaryObj.getString("summaryText"));
						summaryBean.setUserName(summaryObj.getString("userName"));
						summaryBean.setInsertDate(summaryObj.getDate("insertDate"));
						
						summaries.add(summaryBean);
					}
				}
				incident.setSummaries(summaries);
			}
			
			if (null != dbObject.get("discussions")) {
				BasicDBList  discussionList = (BasicDBList)dbObject.get("discussion");
				
				Collection<DiscussionBean> discussions = new ArrayList<DiscussionBean>();
				BasicDBObject discussionObj = null;
				DiscussionBean discussionBean = null;
				for (int i=0;i<discussionList.size();i++) {
					
					discussionObj = (BasicDBObject)discussionList.get(i);
					if (null != discussionObj) {
						discussionBean = new DiscussionBean();
						discussionBean.setSummaryText(discussionObj.getString("summaryText"));
						discussionBean.setUserName(discussionObj.getString("userName"));
						discussionBean.setInsertDate(discussionObj.getDate("insertDate"));
						
						discussions.add(discussionBean);
					}
				}
				incident.setDiscussions(discussions);
				
			}
*/			
		}
		
		return dbObject.toString();
	}

	public static void createNewIncident(String incidentNumber){
		DBCollection incidentCollection = incidentistDB.getCollection("incidents");
		
		BasicDBObject dbObj = new BasicDBObject();
		dbObj.append("incidentId", incidentNumber);
		
		incidentCollection.insert(dbObj);
	}

	public static String addDiscussion(DiscussionBean discussionBean, String incidentNumber){
		DBCollection incidentCollection = incidentistDB.getCollection("incidents");
		
		DBCursor cursor = incidentCollection.find(new BasicDBObject("incidentId",incidentNumber));

		DBObject dbObject = null;
		while (cursor.hasNext()) {
			dbObject = cursor.next();
			
			
		}		
		
		return getIncident(incidentNumber);
	}

	
	public static void main(String args[]) {
		System.out.println(getIncident("INC1192711"));
		
/*		System.out.println("Incident ID: " + incident.getIncidentId());
		System.out.println("Summaries : " + incident.getSummaries());
		System.out.println("Discussions: "+ incident.getDiscussions());*/
		
		//createNewIncident("dummyIncident");
		
	}
	
/*	public static void saveIncident(String collectionName, BasicDBObject basicDBObject) {
		DBCollection myCollection = incidentistDB.getCollection(collectionName);
		
		myCollection.insert(basicDBObject);
	}

	public static void updateIncident(String collectionName, BasicDBObject basicDBObject) {
		DBCollection myCollection = incidentistDB.getCollection(collectionName);
		
		myCollection.update(basicDBObject);
	}
	
	
	public static void getObjects(String collectionName, BasicDBObject basicDBObject) {
		DBCollection myCollection = incidentistDB.getCollection(collectionName);
		
	}
*/	
	
}
