package com.wellmanage.ic.database;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface IncidentRepository extends MongoRepository<IncidentBean, String>{

}
