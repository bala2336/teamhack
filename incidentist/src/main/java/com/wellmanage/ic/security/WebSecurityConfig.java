package com.wellmanage.ic.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.AuthenticationEntryPoint;

import com.wellmanage.weos.sso.hantweb.EnableHantwebSSOSecurity;
import com.wellmanage.weos.sso.hantweb.HantwebSSOConfigurer;
import com.wellmanage.weos.sso.hantweb.HantwebUserDetailsService;

/**
 * The Class WebSecurityConfig.
 */
@Configuration
@EnableWebSecurity
@ComponentScan
@EnableHantwebSSOSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	/**
     * 2.) Provide the ServiceNow appId
     */
    @Value("${hantweb.appId}")
    private String appId;
    /**
     * 3.) Specify the target service hostname. If not specified,
     * The subject value that the app expects to find in the JWT
     */
    @Value("${hantweb.targetService}")
    private String targetService;

    /**
     * 4.) Define ehe expected issuer value the app expects to find in the JWT
     */
    @Value("${hantweb.host}")
    private String hantwebHost;

    /**
     * 5.) Provide a comma-delimited list of AD roles that will get passed to Hantweb.
     */
    @Value("${hantweb.roles}")
    private String[] hantwebRoles;

    /**
     * 6.) Inject the user details service. By default, this is injected
     * by the {@link EnableHantwebSSOSecurity} annotation.
     */
    @Autowired
    private HantwebUserDetailsService userDetailsService;
	
	
	/** The hantweb authentication entry point. */
	private AuthenticationEntryPoint hantwebAuthenticationEntryPoint = null;

	/** The hantweb authentication manager. */
	private AuthenticationManager hantwebAuthenticationManager = null;

	/** The hantweb pre authenticated processing filter. */
	private final HantwebPreAuthenticatedProcessingFilter hantwebPreAuthenticatedProcessingFilter;

	/**
	 * Instantiates a new web security config.
	 */
	public WebSecurityConfig() {

		super();
		hantwebPreAuthenticatedProcessingFilter = new HantwebPreAuthenticatedProcessingFilter();
		hantwebPreAuthenticatedProcessingFilter
				.setAuthenticationManager(hantwebAuthenticationManager);
		hantwebPreAuthenticatedProcessingFilter
				.setInvalidateSessionOnPrincipalChange(true);
		hantwebPreAuthenticatedProcessingFilter
				.setContinueFilterChainOnUnsuccessfulAuthentication(Boolean.FALSE);

	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter#configure(org.springframework.security.config.annotation.web.builders.HttpSecurity)
	 */
/*	@Override
	protected void configure(HttpSecurity http) throws Exception {

		// @formatter:off
		http.authorizeRequests()
				.antMatchers("/**", "/loggedInUser", "/chat/**", "/error",
						"/unauth", "/redirect", "*support*", "/favicon.ico",
						"/metadata/**", "/bower_components/**", "/app/**",
						"/assets/**", "/api/**", "/images/**", "/topic/**")
				.permitAll()
				.anyRequest()
				.authenticated()
				.and()
				.rememberMe()
				.and()
				.httpBasic()
				.authenticationEntryPoint(hantwebAuthenticationEntryPoint)
				.and()
				.addFilterAfter(hantwebPreAuthenticatedProcessingFilter,
						HantwebPreAuthenticatedProcessingFilter.class).csrf()
				.disable().logout().deleteCookies("JSESSIONID")
				.logoutSuccessUrl("/logoff").invalidateHttpSession(true);
		// @formatter:on
	}*/

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		// @formatter:off
		http
				.csrf().disable()
				.authorizeRequests()				
				.anyRequest().authenticated()
				.anyRequest().hasAnyRole(hantwebRoles)
					.and()
				.apply(HantwebSSOConfigurer.configure(hantwebHost))
                        .appId(appId)
                        .targetService(targetService)
                        .roles(hantwebRoles)
                        .authenticatedUserDetailsService(userDetailsService);
		// @formatter:on
	}	
	
	/**
	 * Sets the hantweb authentication manager.
	 *
	 * @param hantwebAuthenticationManager
	 *            the new hantweb authentication manager
	 */
	@Autowired
	public void setHantwebAuthenticationManager(
			AuthenticationManager hantwebAuthenticationManager) {

		this.hantwebAuthenticationManager = hantwebAuthenticationManager;
		hantwebPreAuthenticatedProcessingFilter
				.setAuthenticationManager(this.hantwebAuthenticationManager);
	}

	/**
	 * Sets the hantweb authentication entry point.
	 *
	 * @param hantwebAuthenticationEntryPoint
	 *            the new hantweb authentication entry point
	 */
	@Autowired
	public void setHantwebAuthenticationEntryPoint(
			AuthenticationEntryPoint hantwebAuthenticationEntryPoint) {

		this.hantwebAuthenticationEntryPoint = hantwebAuthenticationEntryPoint;
	}

}