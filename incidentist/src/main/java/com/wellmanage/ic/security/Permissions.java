package com.wellmanage.ic.security;

public enum Permissions {

	READ_ONLY("READ_ONLY_PRIV"), EDITOR("EDITOR_PRIV"), ADMIN("ADMIN_PRIV");

	private UserPriv value;

	private Permissions(String value) {
		this.value = new UserPrivImpl(value);
	}

	public UserPriv getValue() {
		return value;
	}

}
