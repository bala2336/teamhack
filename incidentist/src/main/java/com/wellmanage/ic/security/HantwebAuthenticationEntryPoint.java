package com.wellmanage.ic.security;

import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Service;

import com.wellmanage.ic.ApplicationConfig;


/**
 * The Class HantwebAuthenticationEntryPoint.
 */
@ComponentScan
@Service
public class HantwebAuthenticationEntryPoint implements AuthenticationEntryPoint {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(HantwebAuthenticationEntryPoint.class);

    /** The Constant HANTWEB_URL. */
    private static final String HANTWEB_URL = "http://hantweb.wellmanage.com/authenticate/encdefault.asp?location=";

    /** The Constant ENCODING. */
    private static final String ENCODING = "UTF-8";

    /** The Constant QUERY_DELIMITER. */
    private static final String QUERY_DELIMITER = "?";

    /** The Constant HEADER_AJAX_CALLS_KEY. */
    private static final String HEADER_AJAX_CALLS_KEY = "accept";

    /** The Constant HEADER_AJAX_CALLS_VALUE. */
    private static final String HEADER_AJAX_CALLS_VALUE = "application/json";

    @Autowired
    private ApplicationConfig applicationConfig;

    /**
     * {@inheritDoc}
     *
     * @see org.springframework.security.web.AuthenticationEntryPoint#commence(javax.servlet.http.HttpServletRequest,
     *      javax.servlet.http.HttpServletResponse, org.springframework.security.core.AuthenticationException)
     */
    @Override
    public void commence(HttpServletRequest aRequest, HttpServletResponse aResponse, AuthenticationException aException) throws IOException, ServletException {

        StringBuilder url = new StringBuilder(HANTWEB_URL);
        String requestURL = aRequest.getRequestURL().toString();
        LOGGER.info(" - Authenticating for  {}", requestURL);
        if (isAjaxRequest(aRequest)) {
            LOGGER.info("Ajax request sent for authentication. {}", requestURL);
            aResponse.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Redirect AJAX request due to invalid session");
        } else {
            String returnURL = getReturnURL(aRequest);
            url.append(URLEncoder.encode(returnURL, ENCODING));
            LOGGER.info(" - Sending redirect to " + url.toString());
            aResponse.sendRedirect(url.toString());
        }
    }

    /**
     * Checks if is ajax request.
     *
     * @param request the request
     * @return true, if is ajax request
     */
    private boolean isAjaxRequest(HttpServletRequest request) {

        String requestType = request.getHeader(HEADER_AJAX_CALLS_KEY);
        return StringUtils.contains(requestType, HEADER_AJAX_CALLS_VALUE);
    }

    /**
     * Gets the return url.
     *
     * @param request the a request
     * @return the return url
     */
    private String getReturnURL(HttpServletRequest request) {

        String port = applicationConfig.getApplicationPort();
        String queryString = request.getQueryString();
        String url = request.getRequestURL().toString();
        
        if (StringUtils.isNotEmpty(port)) {
            url = StringUtils.replace(url, ":" + port, StringUtils.EMPTY);
            LOGGER.info("port {} for tomcat. new url {}", port, url);
        } else {
            LOGGER.info("No port defined for tomcat.  Using default url {}", url);
        }

        StringBuilder result = new StringBuilder(url);
        
        if (queryString != null) {
            result.append(QUERY_DELIMITER).append(request.getQueryString());
        }

        StringBuilder requestedUrl = new StringBuilder(url.substring(0, url.indexOf("/")));
        requestedUrl.append("/logUserDetail");
        requestedUrl.append(QUERY_DELIMITER).append("redirectUrl=").append(result.toString());
                
        return requestedUrl.toString();
    }
}
