package com.wellmanage.ic.security;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.wellmanage.edc.ds.wal.client.Entitlements;
import com.wellmanage.edc.ds.wal.client.WalEntitlement;

/**
 * The Class WALAuthentication.
 */
@Repository
@Service
public class WALAuthentication {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(WALAuthentication.class);

    /** The Constant WAL_URL_KEY. */
    private static final String WAL_URL_KEY = "wal.url";

    /** The Constant WAL_APP_ID. */
    private static final String WAL_APP_ID = "MNPI";

    /** The environment. */
    private Environment environment;

    /**
     * Gets the WAL entitlements.
     *
     * @param userLogin the user login
     * @return the WAL entitlements
     */
    public Collection<UserPriv> getWALEntitlements(String userLogin) {

        String walURL = environment.getRequiredProperty(WAL_URL_KEY);
        WalEntitlement wal = new WalEntitlement();
        wal.setWalHost(walURL);
        wal.setWalAppId(WAL_APP_ID);
        Entitlements entitlement = wal.getEntitlements(userLogin);
        LOGGER.info("### WAL call for {}, results:{}", walURL, entitlement);

        Collection<UserPriv> userPrivs = new ArrayList<UserPriv>();

        if (entitlement == null || CollectionUtils.isEmpty(entitlement.getPrivileges())) {
            String warning = "User " + userLogin + " is not authorized";
            LOGGER.warn(warning);
        } else {
            for (String priv : entitlement.getPrivileges()) {
                userPrivs.add(new UserPrivImpl(priv));
            }
        }

        return userPrivs;
    }

    /**
     * Sets the environment.
     *
     * @param environment the new environment
     */
    @Autowired
    public void setEnvironment(Environment environment) {

        this.environment = environment;
    }
}
