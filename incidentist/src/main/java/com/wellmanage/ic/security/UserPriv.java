package com.wellmanage.ic.security;

import org.springframework.security.core.GrantedAuthority;

/**
 * The Interface UserPriv.
 */
public interface UserPriv extends GrantedAuthority {

}