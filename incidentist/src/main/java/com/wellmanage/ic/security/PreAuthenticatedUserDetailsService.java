package com.wellmanage.ic.security;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Component;

@Component("preAuthenticatedUserDetailsService")
public class PreAuthenticatedUserDetailsService implements
		AuthenticationUserDetailsService<PreAuthenticatedAuthenticationToken> {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(PreAuthenticatedUserDetailsService.class);

	/** The wal authentication. */
	@Autowired
	private WALAuthentication walAuthentication;

	@Override
	public UserDetails loadUserDetails(PreAuthenticatedAuthenticationToken token)
			throws UsernameNotFoundException {

		// TODO Auto-generated method stub
		HantwebPrincipal principal = (HantwebPrincipal) token.getPrincipal();
		LOGGER.info("Authenticate: " + principal);
		Collection<UserPriv> userPrivs = walAuthentication
				.getWALEntitlements(principal.getUserLogin());

		User usr = new User(principal.getUserLogin(), "N/A", userPrivs);

		return usr;

	}

}
