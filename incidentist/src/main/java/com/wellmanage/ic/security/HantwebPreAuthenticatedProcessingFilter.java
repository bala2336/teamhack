package com.wellmanage.ic.security;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

/**
 * The Class HantwebPreAuthenticatedProcessingFilter.
 */
public class HantwebPreAuthenticatedProcessingFilter extends AbstractPreAuthenticatedProcessingFilter {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(HantwebPreAuthenticatedProcessingFilter.class);

    /** The Constant HANTWEB_DECRYPT. */
    private static final String HANTWEB_DECRYPT = "http://hantweb.wellmanage.com/authenticate/decrypt.asp?token=";

    /** The Constant USER_AGENT. */
    private static final String USER_AGENT = "Mozilla/5.0";

    /**
     * Decrypt token.
     *
     * @param token the token
     * @return the string
     */
    private String decryptToken(String token) {

        String decryptedToken = null;
        String url = null;
        try {
            url = HANTWEB_DECRYPT + token;

            URL obj = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) obj.openConnection();

            // optional default is GET
            connection.setRequestMethod("GET");

            // add request header
            connection.setRequestProperty("User-Agent", USER_AGENT);

            int responseCode = connection.getResponseCode();
            LOGGER.info("Sending request to URL: {}, response:{}", url, responseCode);

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = bufferedReader.readLine()) != null) {
                response.append(inputLine);
            }
            bufferedReader.close();

            // print result
            decryptedToken = response.toString();
        } catch (Exception e) {
             LOGGER.error("Unable to decrypt hantweb token from URL:" + url + " Exception:" + e);
        }

        return decryptedToken;

    }

    /*
     * (non-Javadoc)
     * @see
     * org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter#getPreAuthenticatedCredentials
     * (javax.servlet.http.HttpServletRequest)
     */
    /**
     * {@inheritDoc}
     * @see org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter#getPreAuthenticatedCredentials(javax.servlet.http.HttpServletRequest)
     */
    @Override
    protected Object getPreAuthenticatedCredentials(HttpServletRequest request) {

        return "N/A";
    }

    /*
     * (non-Javadoc)
     * @see
     * org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter#getPreAuthenticatedPrincipal
     * (javax.servlet.http.HttpServletRequest)
     */
    /**
     * {@inheritDoc}
     * @see org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter#getPreAuthenticatedPrincipal(javax.servlet.http.HttpServletRequest)
     */
    @Override
    protected Object getPreAuthenticatedPrincipal(HttpServletRequest request) {

        String token = request.getParameter("token");
        String ntLogin = request.getParameter("ntLogin");
        String timeStamp = request.getParameter("ts");

        LOGGER.debug("token={} ntLogin={} timeStamp:{}", token, ntLogin, timeStamp);
        if (StringUtils.isNotEmpty(token) && StringUtils.isNotEmpty(ntLogin)) {
            String decryptedToken = decryptToken(token);
            String decryptedLogin = decryptToken(ntLogin);

            if (StringUtils.isEmpty(decryptedToken) || StringUtils.isEmpty(decryptedLogin)) {
                // sending the user to hantweb. Upon return, the ntLogin parameter would be set
                LOGGER.error("No decryptedToken found: decryptedToken=" + decryptedToken + " decryptedLogin:" + decryptedLogin);
                return null;
            } else if (StringUtils.equals(decryptedLogin, decryptedToken)) {
                HantwebPrincipal principal = new HantwebPrincipal(decryptedLogin, timeStamp);
                LOGGER.debug("principal = {}",principal);
                return principal;
            } else {
                LOGGER.error("decryptedToken:{} does not matchd ecryptedLogin:{} ",decryptedLogin, decryptedToken);
                return null;
            }

        } else {
            // If the ntLogin parameter is null, the spring infrastructure will end up invoking the HantwebAuthenticationEntryPoint,
            // sending the user to hantweb. Upon return, the ntLogin parameter would be set
            LOGGER.debug("No principal information found.  Returning null.");
            return null;
        }
    }
}
