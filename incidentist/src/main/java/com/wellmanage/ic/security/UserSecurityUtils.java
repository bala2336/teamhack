package com.wellmanage.ic.security;

import java.util.Collection;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;

/**
 * The Class UserSecurityUtils.
 */
@Configuration
@Repository
public class UserSecurityUtils {
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(UserSecurityUtils.class);

	@SuppressWarnings("unchecked")
	public Collection<UserPriv> getUserPrvis() {
		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		return (Collection<UserPriv>) authentication.getAuthorities();
	}

/*	public HantwebPrincipal getUser((@AuthenticationPrincipal HantwebUser user) {

		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		HantwebPrincipal principal;
		if (authentication.getPrincipal() instanceof String) {
			principal = new HantwebPrincipal(" ", " ");
		} else {
			principal = (HantwebPrincipal) authentication.getPrincipal();
		}
		return principal;
	}*/

	/**
	 * Logout user.
	 * 
	 * @param session
	 */
	public void logoutUser(HttpSession session) {
		SecurityContextHolder.getContext().getAuthentication()
				.setAuthenticated(false);
		SecurityContextHolder.getContext().setAuthentication(null);
	}

	/**
	 * Checks if is access granted.
	 *
	 * @return true, if is access granted
	 */
	public boolean isAccessGranted() {

		boolean accessGranted = false;
		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		if (authentication != null) {
			HantwebPrincipal principal = (HantwebPrincipal) authentication
					.getPrincipal();
			if (principal != null) {
				if (!accessGranted) {
					LOGGER.info("Access denied for {}. No privs found",
							principal.getUserLogin());
				}
			}
		}
		return accessGranted;
	}

}