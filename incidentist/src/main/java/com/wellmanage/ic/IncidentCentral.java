package com.wellmanage.ic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IncidentCentral {

    public static void main(String[] args) {
        SpringApplication.run(IncidentCentral.class, args);
    }
}