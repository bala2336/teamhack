angular.module('MetronicApp').filter(
		'propsFilter',
		function() {
			return function(items, props) {
				var out = [];

				if (angular.isArray(items)) {
					items.forEach(function(item) {
						var itemMatches = false;

						var keys = Object.keys(props);
						for (var i = 0; i < keys.length; i++) {
							var prop = keys[i];
							var text = props[prop].toLowerCase();
							if (item[prop].toString().toLowerCase().indexOf(
									text) !== -1) {
								itemMatches = true;
								break;
							}
						}

						if (itemMatches) {
							out.push(item);
						}
					});
				} else {
					// Let the output be the input untouched
					out = items;
				}

				return out;
			};
		});

angular
		.module('MetronicApp')
		.controller(
				'teamsCtrl',
				function($scope, $http, $timeout) {
					$scope.$on('$viewContentLoaded', function() {
						App.initAjax(); // initialize core components
					});
					$scope.submitTeamUsers = function() {
						var names = [];
						for (var i = 0; i < $scope.multipleDemo.selectedPeople.length; i++) {
							names
									.push($scope.multipleDemo.selectedPeople[i].name);
						}
						get();
					}
					$scope.resetTeamUsers = function() {
						$scope.multipleDemo.selectedPeople = [];
					}

					function get() {
						function success(result) {
							return result;
						}
						function error(err) {
							return err.data;
						}
						return $http({
							url : "/api/sendXmatters",
							method : "GET",
							timeout : 200000
						}).then(success, error);
					}

					function post(restUrl, data) {
						function success(result) {
							notify.log('POST', restUrl, result);
							// application level info/errors
							showMessage(result.data)
							return result.status;
						}
						function error(err) {
							// server level errors
							errorHandling(err.status)
							return err.data;
						}
						return $http.post(restUrl, data).then(success, error);
					}

					$scope.showMessage = function() {
						$scope.message = "Notification has been sent";
					}
					$scope.disabled = undefined;
					$scope.searchEnabled = undefined;

					$scope.enable = function() {
						$scope.disabled = false;
					};

					$scope.disable = function() {
						$scope.disabled = true;
					};

					$scope.enableSearch = function() {
						$scope.searchEnabled = true;
					}

					$scope.disableSearch = function() {
						$scope.searchEnabled = false;
					}

					$scope.clear = function() {
						$scope.person.selected = undefined;
						$scope.address.selected = undefined;
						$scope.country.selected = undefined;
					};

					$scope.someGroupFn = function(item) {

						if (item.name[0] >= 'A' && item.name[0] <= 'M')
							return 'From A - M';

						if (item.name[0] >= 'N' && item.name[0] <= 'Z')
							return 'From N - Z';

					};

					$scope.personAsync = {
						selected : "wladimir@email.com"
					};
					$scope.peopleAsync = [];

					$timeout(function() {
						$scope.peopleAsync = [ {
							name : 'VenkaK',
							email : 'venkataraman@wellington.com',
							age : 12,
							country : 'United States'
						}, {
							name : 'NarayA',
							email : 'ANarayan@wellington.com',
							age : 12,
							country : 'Argentina'
						} ];
					}, 3000);

					$scope.counter = 0;
					$scope.someFunction = function(item, model) {
						$scope.counter++;
						$scope.eventResult = {
							item : item,
							model : model
						};
					};

					$scope.removed = function(item, model) {
						$scope.lastRemoved = {
							item : item,
							model : model
						};
					};

					$scope.person = {};
					$scope.people = [ {
						name : 'VenkaK',
						email : 'venkataraman@wellington.com',
						age : 12,
						country : 'United States'
					}, {
						name : 'NarayA',
						email : 'ANarayan@wellington.com',
						age : 12,
						country : 'Argentina'
					} ];
					$scope.availableColors = [ 'Red', 'Green', 'Blue',
							'Yellow', 'Magenta', 'Maroon', 'Umbra', 'Turquoise' ];
					$scope.multipleDemo = {};
					$scope.multipleDemo.colors = [ 'Blue', 'Red' ];
					$scope.multipleDemo.selectedPeople = [];

					$scope.address = {};
					$scope.refreshAddresses = function(userName) {
						var params = {
							userPattern : userName
						};
						return $http.get('/api/serviceNow/findUser', {
							params : params
						}).then(function(response) {
							$scope.addresses = response.data.results;
						});
					};

					$scope.getUsers = function(val) {
						return $http.get('/api/serviceNow/findUser', {
							params : {
								userPattern : val
							}
						}).then(function(res) {
							var foundIssuers = {};
							var result = [];
							angular.forEach(res.data, function(item) {
								angular.forEach(item, function(itm) {
									foundIssuers[item.id] = itm.user_name;
								});
							});
							angular.forEach(foundIssuers, function(item) {
								result.push(item);
							});
							return result;
						});

					}
				});