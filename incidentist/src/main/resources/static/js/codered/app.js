var codered = angular.module('codered',['ui.tinymce']);
codered.controller('tinymceCtrolor', function($scope) {
	  $scope.tinymceOptions = {
			    onChange: function(e) {
			      // put logic here for keypress and cut/paste changes
			    },
			    inline: false,
			    plugins : 'advlist autolink link image lists charmap print preview',
			    skin: 'lightgray',
			    theme : 'modern'
			  };
			});